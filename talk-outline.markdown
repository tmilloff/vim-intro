# Preamble
  * Introduce self (Jason M, Software Eng @ Triad Interactive, tmilloff@radford.edu)
  * Brief history of Vim
      ed -> ex -> vi -> vim
  * Author/Maintainer: Bram Moolenar

## About the talk
  * This outline availible here: https://bitbucket.org/tmilloff/vim-intro
  * I tried to cram a lot in here
  * Don't worry about remembering everything
  * Get a basic idea of Vim's functionality
  * Start with remembering a small set of features
  * Add more over time
  * This outline along with demo vimrc (vimrc is vim config file) are up on github
  * Hold questions until the end unless I say something really confusing
  * You can email me if you have any questions later
  * TODO Explain screenkey (maybe don't use it?)

# Why vim
  Speed:
    * Text editing is fast and efficient
    * Vim starts up very fast
    * Often available on remote systems
    * Active updates (Just recently got terminal in vim support)
    * Great documentation
    * Really convenient when doing other stuff in the terminal (ex. git)
    * Can do everything your text editor/IDE does and more

# Modes
  * Vim has multiple modes
  * Keys have different functions depending on the mode
  * Most common modes: Normal, Command Line, Insert, Visual
## Normal Mode Summary
  * Cursor movement, scrolling
    * Mention hjkl arrows
  * Perform operations on text
  * Transition point to/from other modes
  * Most time is spent here
## Insert Mode Summary
  * Inserting text
  * That's it (though there are nice keyboard shortcuts in this mode)
## Command Line Mode Summary
  * Run Ex commands (usually just called commands)
  * Change settings
  * Searching text
## Visual Mode Summary
  * Create/adjust selections before operating on them
  * Three ways to select text! (Visual, Visual Line, Visual Block)

# Opening and closing vim
  * vim [file]
  * Opens in Normal mode
  * Quitting: [w]q[a][!]

# Editing text
  * Vim starts in normal mode
  * Navigate around in normal mode
    * hjkl
    * w b e ge
    * Ctrl-F Ctrl-B
    * Ctrl-D Ctrl-U
    * Most movements can be modified with numbers 3w 3j 3f !!
  * Getting in and out of insert mode: i a <Esc>
  * Yanking/deleting: copying/cutting on steroids
    * Vim has multiple 'clipboards' called registers
      * "{char}y
      * Default/unnamed register is called "
      * Deleting puts text in register
      * Yanked text is stuffed in " AND 0 register
      * Delete doesn't touch 0 register
      * :reg command
      * Access system clipboard with + and * registers
  * Change command: delete and then go into insert mode
  * finding text with / ? n N
    Relevant settings
    " Highlight search results
    set hlsearch
    " Show the first match while typing the search
    set incsearch
    " Ignore case when search term is lower case
    set ignorecase
    set smartcase
  * Find and replace with :s
  * Find lines and run commands with :g
    :%g/pattern/excommand
    * Run ex commands on lines that match a pattern
    * Can run normal mode commands with :normal

  * Vim has Tabs and Splits!
    * :tabnew gt gT {n}gt
    * :split :vsplit Ctrl-W s  Ctrl-W v
    * Ctrl W hjkl

  * Vim has a spell checker: set spell

  * Auto completions!
    * Ctrl-n and Ctrl-p
    * Mention my script for this
    * Plugins: Supertab, YCM, Neocomplete
    * Line complete: Ctrl-X Ctrl-L

  * Vim's Undo is the best!
    * u and Ctrl-r
    * Vim uses a tree instead of a stack (!!)
    * undol - gross :(
    * Undotree - pretty :)
    * Persistent undo option

# Vimrc file / Configuring vim
  * File called .vimrc in your home directory
  * Runs command line mode commands on load, that’s it
  * Any command you can run in command line mode can be placed in vimrc
  * You probably wanna version control this file (git)
  * Already mentioned search settings
  * set backspace=indent,eol,start to make insert mode behave like other editors
  * set wildmenu (may be on by default)
  * set showmode
  * set nowrap (if you dislike word wrapping like me)
  * (Controversial) mouse support
    set mouse=a
    if has("mouse_sgr")
      set ttymouse=sgr
    else
      set ttymouse=xterm2
    end
  * Indenting!
      " Make a Tab look like 2 spaces
      set tabstop=2
      " Backspace over 2 spaces as if they were one tab
      set softtabstop=2
      " Auto indent two spaces per level
      set shiftwidth=2
      " Insert spaces instead of tabs when pressing <Tab>
      set expandtab
      " Generally tabstop and shiftwidth should be the same
      " softtabstop of 0 when using noexpandtab

      " autocommands run a command automatically when a condition is met
      " putting them in augroups with autocmd! inside prevents trouble when reloading your vimrc
      augroup indents
        autocmd!
        " Indent with a TAB that looks like 4 spaces in java
        autocmd Filetype java setlocal ts=4 sts=0 sw=4 noexpandtab
        " Indent with 2 spaces in css
        autocmd Filetype css setlocal ts=2 sts=2 sw=2 expandtab
      augroup END
  * Make
    * make <args> run the command set via 'set makeprg=...'
    * For example
        set makeprg=javac\ *.java
       or
      augroup make
        autocmd!
        " Use javac as the makeprg when in a java file
        autocmd Filetype java setlocal makeprg=javac\ *.java
      augroup END
  * Using quickfix and location lists
    * Mention my custom mappings for navigating qf and ll
  * Custom mappings and commands
    * map/noremap
      Common remappings:
      Ctrl HJKL split navigation
      jk instead of escape
      Mention rebinding CapsLock
      ; instead of :
    * Leader key: let mapleader="<KEY>"
    * command! Name command
    * abbreviate

  * Plugins!
    * Probably wanna use a plugin manager (I use vim-plug)
    * Some plugins I find essential for IDE-like behavior
      * Airline     (Better status line)
      * vim-vinegar (Better key bindings for vim's built in file manager)
      * startify    (Better startup screen)
      * undotree    (Better UI for managing undos)
      * fzf         (Fuzzy finder for finding files)
      * Asyncrun    (Run any command async and put output in qf window)
      * Ale         (Lints your code in real time)
      * Themes      (I use solarized-dark most of the time)
      * syntax files for your language (May not be needed, vim supports a lot by default)

# Navigating through projects
  * Netrw and vim-vinegar
    * :Explore / :Sexplore
  * NERDtree
  * FZF (Good place to mention cd, lcd, pwd, %:p:h)
  * vimgrep /pattern/ {files}
      vimgrep /String/ *.java
  * Asyncrun with Ripgrep

# Moving around and deleting/changing text quickly
  * Try to be brief here, this stuff is all in vim tutor/vim cheatsheets
  * Arrow keys already mentioned
  * Delete/change whole line, rest of line: D C dd cc
  * Text Objects! So powerful
    * Change a word: caw
    * Change in parens: ci(
    * Change in quotes: ci"
  * Entering insert mode with i, a, I, A
    * Deleting words with Ctrl W in insert mode
    * Paste from a reg in insert mode with Ctrl-R {reg}
  * Join lines with J
  * New line with o and O
  * Indent with >, <, >>, <<, =, == (Visual mode is good for this too)
  * Wordwise movement: w b e ge W B E gE ; ,
  * Paragraph and sentence { } ( )
  * Finding on the line: f t F T ; ,
  * Start and End of Line: 0 ^ $
  * Moving to other bracket: %
  * Highest Middle and Lowest lines in window: H M L
  * Scroll down with Ctrl E / Y
  * Move current line to top, middle, bottom of screen
    * zt
    * zz
    * zb
  * Scrolling Window: C-d C-u C-f C-b
  * Marking with m{char}, and jumping back with `{char} or '{char} !!
  * Jump List! Most commands that move the cursor populate a 'jump list' so you can return the cursor to prev position
    Ctrl I / Ctrl O
  * Jump to tag very helpful in vim's help system: Ctrl ]
  * Repeat last edit with .
  * Macros: q and @
  * Command window: q:
  * Search window: q/ q?

# Visual Mode
  * v - visual
  * V - Visual Line
  * Ctrl V - Visual block
  * gv - Reuse last selection
  * gn,gN - Select next match from last search
## Visual Mode keys
  * o, O, $
  * d, c, y, <, >, !, =
  * Sort a selection with !sort

# Getting Help and Learning More
  * help command
  * Ctrl ] / Ctrl O / Ctrl I
  * helpgrep
  * help user-manual
  * vimtutor
  * Google (will probably take you to one of the below)
  * StackOverflow
  * r/vim

# Further Reading
  * This talk + demo vimrc and my vimrc: https://bitbucket.org/tmilloff/vim-intro
  * More information on vim's registers: http://www.brianstorti.com/vim-registers/
  * Good info on settings and using vimrc: https://github.com/romainl/idiomatic-vimrc/blob/master/idiomatic-vimrc.vim
  * Setting up FZF and Ripgrep: https://medium.com/@crashybang/supercharge-vim-with-fzf-and-ripgrep-d4661fc853d2
  * Vim movements cheatsheet: https://i.imgur.com/CjREO9t.png
  * Great talk on vim's autocomplete functionality: https://www.youtube.com/watch?v=3TX3kV3TICU
  * Huge collection of screencasts/tutorials by an eminent vim user: http://vimcasts.org
